/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.LyricsDAO;
import dal.SongsDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Lyrics;
import model.Users;

/**
 *
 * @author Thanh Thao
 */
public class AddSongsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddSongsServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddSongsServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Users u = (Users) session.getAttribute("user");
        if (u.getRole_ID() != 1 && u == null) {
            PrintWriter out = response.getWriter();
            out.println("access denied");
        } else {
            response.setContentType("text/html;charset=UTF-8");
            request.setCharacterEncoding("UTF-8");

            String name = request.getParameter("name");
            String image = request.getParameter("image");
            String author = request.getParameter("author");
            String duration = request.getParameter("duration");
            String path = request.getParameter("path");
            String timeCreate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
            int categoryID = Integer.parseInt(request.getParameter("category"));

            SongsDAO dao = new SongsDAO();
            int song_ID = dao.insertSongs(name, author, duration, image, timeCreate, path, categoryID);
            String lyric = request.getParameter("lyric");
            if (lyric.length() > 8) {
                String[] lyrics = lyric.trim().split("\n");
                LyricsDAO ld = new LyricsDAO();
                for (String item : lyrics) {
                    Lyrics l = new Lyrics();
                    l.setTime(item.trim().substring(1, 6));
                    l.setContent(item.trim().substring(7));
                    l.setSongID(song_ID);
                    ld.insertLyric(l);
                }
            }
            if (u.getRole_ID() == 3) {
                int songID = dao.getAll().get(dao.getAll().size() - 1).getSong_ID();
                // temporary solution, not including the case that songs are added at the same time
                dao.insertHistory(name, timeCreate, u.getUser_ID(), songID);
            }
            String mess = "Add Successfully!";
            session.setAttribute("mess", mess);
            response.sendRedirect("manager");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
