/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author huanv
 */
public class HistoryView {
    private int history_ID;
    private Timestamp t_create;
    private int user_ID;
    private int song_ID;
    
    private String username;
    private String name;
    private String author;
    private String duration;
    private String image;

    public HistoryView() {
    }

    public HistoryView(int history_ID, Timestamp t_create, int user_ID, int song_ID) {
        this.history_ID = history_ID;
        this.t_create = t_create;
        this.user_ID = user_ID;
        this.song_ID = song_ID;
    }

    public HistoryView(int history_ID, Timestamp t_create, int user_ID, int song_ID, String username, String name, String author, String duration, String image) {
        this.history_ID = history_ID;
        this.t_create = t_create;
        this.user_ID = user_ID;
        this.song_ID = song_ID;
        this.username = username;
        this.name = name;
        this.author = author;
        this.duration = duration;
        this.image = image;
    }
    
    public String getUsername() {
        return username;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getHistory_ID() {
        return history_ID;
    }

    public void setHistory_ID(int history_ID) {
        this.history_ID = history_ID;
    }

    public Timestamp getT_create() {
        return t_create;
    }

    public void setT_create(Timestamp t_create) {
        this.t_create = t_create;
    }

    public int getUser_ID() {
        return user_ID;
    }

    public void setUser_ID(int user_ID) {
        this.user_ID = user_ID;
    }

    public int getSong_ID() {
        return song_ID;
    }

    public void setSong_ID(int song_ID) {
        this.song_ID = song_ID;
    }
    
}
