/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author Admin
 */
public class Albums {
    private int albums_ID;
    private String image;
    private String description;
    private String name;
    private int userid;
    private Songs song;
    private Users user;
    private Timestamp t_create;

    public Albums() {
    }
    
    public Albums(int albums_ID, String name, Timestamp t_create) {
        this.albums_ID = albums_ID;
        this.name = name;
        this.t_create = t_create;
    }
    
    public Albums(int albums_ID, String image, String name, Timestamp t_create) {
        this.albums_ID = albums_ID;
        this.image = image;
        this.name = name;
        this.t_create = t_create;
    }

    public Albums(int albums_ID, String name, int userid, Songs song, Users user, Timestamp t_create) {
        this.albums_ID = albums_ID;
        this.name = name;
        this.userid = userid;
        this.song = song;
        this.user = user;
        this.t_create = t_create;
    }

    public Albums(int albums_ID, String image, String description, String name, int userid, Songs song, Users user, Timestamp t_create) {
        this.albums_ID = albums_ID;
        this.image = image;
        this.description = description;
        this.name = name;
        this.userid = userid;
        this.song = song;
        this.user = user;
        this.t_create = t_create;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    

    public int getAlbums_ID() {
        return albums_ID;
    }

    public void setAlbums_ID(int albums_ID) {
        this.albums_ID = albums_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public Songs getSong() {
        return song;
    }

    public void setSong(Songs song) {
        this.song = song;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Timestamp getT_create() {
        return t_create;
    }

    public void setT_create(Timestamp t_create) {
        this.t_create = t_create;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    
}
