/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Users;

/**
 *
 * @author Black
 */
@WebFilter(filterName = "AuthorizationFilter", urlPatterns = {"/admin/*", "/manageAlbum", "/update?id", "/block?id", 
    "/unblock?id", "/deletealbum?id", "/updatealhum?id", "/addsongtoalbum?aid", "/managecomment", "/blockcomment?cid", 
    "/unblockcomment?cid", "/managehistory", "/deleteSCheckbox?music"})
public class AuthorizationFilter implements Filter {// , "/manageLike"

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest reques = (HttpServletRequest) request;
        HttpServletResponse respon = (HttpServletResponse) response;
        HttpSession session = reques.getSession();
        Users user = (Users) session.getAttribute("user");

        if (user != null && user.getRole_ID() == 1) {
            chain.doFilter(request, response);
        }

        reques.setAttribute("error", "You are not permission. Please login with role ADMIN!");
        reques.getRequestDispatcher("login.jsp").forward(request, response);
    }

    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {
    }

    /**
     * Init method for this filter
     *
     * @param filterConfig
     */
    @Override
    public void init(FilterConfig filterConfig) {

    }

}
