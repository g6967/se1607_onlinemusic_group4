/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Slider;

/**
 *
 * @author dclon
 */
public class SliderDAO extends BaseDAO<Slider> {

    @Override
    public ArrayList<Slider> getAll() {
        ArrayList<Slider> sliders = new ArrayList<>();
        try {
            String sql = "select id, song_ID, heading, text  \n" 
                    +    " from Slider ";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                SongsDAO ad = new SongsDAO();
                Slider s = new Slider();
                s.setId(rs.getInt("id"));
                s.setSong(ad.getSongsByID(rs.getInt("song_ID")));
                s.setHeading(rs.getString("heading"));
                s.setText(rs.getString("text"));
                sliders.add(s);
            }
        } catch (Exception e) {
        }
        return sliders;
    }

}
