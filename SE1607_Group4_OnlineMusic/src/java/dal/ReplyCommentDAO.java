/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.ReplyComment;

/**
 *
 * @author Auriat
 */
public class ReplyCommentDAO extends BaseDAO<ReplyComment> {

    public ArrayList<ReplyComment> getReplyComment() {
        ArrayList<ReplyComment> reply = new ArrayList<ReplyComment>();
        try {
            String sql = "Select *\n"
                    + "From ReplyComment where  status = 1\n";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ReplyComment s = new ReplyComment();
                s.setId(rs.getInt(1));
                s.setSong_ID(rs.getInt(2));
                s.setUser_ID(rs.getInt(3));
                s.setComment_ID(rs.getInt(4));
                s.setContent(rs.getString(5));
                s.setT_create(rs.getTimestamp(6));
                s.setStatus(rs.getBoolean(8));
                
                reply.add(s);
            }
        } catch (SQLException ex) {

        }
        return reply;
    }

    public void insertReply(int userID, int commentID, String content, int songID) {
        try {
            String sql = "INSERT INTO [dbo].[ReplyComment]\n"
                    + "           ([song_ID]\n"
                    + "           ,[user_ID]\n"
                    + "           ,[comment_ID]\n"
                    + "           ,[content],[report],[status])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?,0,1)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, songID);
            st.setInt(2, userID);
            st.setInt(3, commentID);
            st.setString(4, content);
            st.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void deleteReply(int replyID) {
        try {
            String sql = "DELETE FROM [dbo].[ReplyComment]\n"
                    + "      WHERE reply_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, replyID);
            st.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    

    @Override
    public ArrayList<ReplyComment> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
