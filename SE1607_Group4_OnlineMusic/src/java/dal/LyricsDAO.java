/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Lyrics;

/**
 *
 * @author Thanh Thao
 */
public class LyricsDAO extends BaseDAO<Lyrics> {

    @Override
    public ArrayList<Lyrics> getAll() {
        ArrayList<Lyrics> lyrics = new ArrayList<>();
        try {
            String sql = "select * from Lyrics ";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Lyrics s = new Lyrics();
                s.setLyricID(rs.getInt(1));
                s.setContent(rs.getString(2));
                s.setTime(rs.getString(3));
                s.setSongID(rs.getInt(4));

                lyrics.add(s);
            }
        } catch (SQLException ex) {

        }
        return lyrics;
    }

    public ArrayList<Lyrics> getLyricsWithSongID(int songID) {
        ArrayList<Lyrics> lyrics = new ArrayList<>();
        try {
            String sql = "select * from Lyrics a inner join Songs b on a.song_ID = b.song_ID and a.song_ID =?";
//             String sql ="select a.time, a.content from Lyrics a inner join Songs b on a.song_ID = b.song_ID where a.song_ID =? ";

            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, songID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Lyrics s = new Lyrics();
                s.setLyricID(rs.getInt(1));
                s.setContent(rs.getString(2));
                s.setTime(rs.getString(3));
                s.setSongID(rs.getInt(4));

                lyrics.add(s);
            }
        } catch (SQLException ex) {

        }
        return lyrics;
    }

    public static void main(String[] args) {
        LyricsDAO ldao = new LyricsDAO();
        System.out.println(ldao.getLyricsWithSongID(110));
    }

    public void insertLyric(Lyrics l) {
        try {
            String sql = "INSERT INTO Lyrics(content, [time], song_ID)\n"
                    + "VALUES(?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, l.getContent());
            st.setString(2, l.getTime());
            st.setInt(3, l.getSongID());
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void removeAllLyricsWithSongID(int songID) {
        try {
            String sql = "delete from Lyrics where song_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, songID);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

}
