package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class songDetail_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_set_var_value_scope_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_if_test;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_set_var_value_scope_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_if_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
    _jspx_tagPool_c_set_var_value_scope_nobody.release();
    _jspx_tagPool_c_if_test.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      if (_jspx_meth_c_set_0(_jspx_page_context))
        return;
      out.write('\r');
      out.write('\n');
      if (_jspx_meth_c_set_1(_jspx_page_context))
        return;
      out.write('\r');
      out.write('\n');
      if (_jspx_meth_c_set_2(_jspx_page_context))
        return;
      out.write('\r');
      out.write('\n');
      if (_jspx_meth_c_set_3(_jspx_page_context))
        return;
      out.write('\r');
      out.write('\n');
      if (_jspx_meth_c_set_4(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n");
      out.write("        <title>Song</title>\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"./css/app.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://pro.fontawesome.com/releases/v5.15.1/css/all.css\" type=\"text/css\">\r\n");
      out.write("        <meta charset=\"utf-8\">\r\n");
      out.write("        <!--  This file has been downloaded from bootdey.com @bootdey on twitter -->\r\n");
      out.write("        <!--  All snippets are MIT license http://bootdey.com/license -->\r\n");
      out.write("        <title>blog item comments - Bootdey.com</title>\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n");
      out.write("        <script src=\"https://code.jquery.com/jquery-1.10.2.min.js\"></script>\r\n");
      out.write("        <link href=\"https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\" rel=\"stylesheet\">\r\n");
      out.write("        <script src=\"https://netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>\r\n");
      out.write("  <style>\r\n");
      out.write("        @media all and (min-width: 992px) {\r\n");
      out.write("            .header__nav .nav-item .dropdown-menu{ display: none; }\r\n");
      out.write("            .header__nav .nav-item:hover .nav-link{   }\r\n");
      out.write("            .header__nav .nav-item:hover .dropdown-menu{ display: block; }\r\n");
      out.write("            .header__nav .nav-item .dropdown-menu{ margin-top:0; }\r\n");
      out.write("        }\r\n");
      out.write("        .slider__item .slider__content {\r\n");
      out.write("            margin-bottom: 500px;\r\n");
      out.write("        }\r\n");
      out.write("        .bg-overlay::after {\r\n");
      out.write("            position: absolute;\r\n");
      out.write("            content: \"\";\r\n");
      out.write("            height: 100%;\r\n");
      out.write("            width: 100%;\r\n");
      out.write("            top: 0;\r\n");
      out.write("            left: 0;\r\n");
      out.write("            z-index: -1;\r\n");
      out.write("            background-color: #7abaff;\r\n");
      out.write("        }\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    </style>\r\n");
      out.write("    </head>\r\n");
      out.write("  \r\n");
      out.write("    <body>\r\n");
      out.write("        <section id=\"header\" style=\"background: #000000\">\r\n");
      out.write("            <div class=\"header-mobile\">\r\n");
      out.write("                <div class=\"mobile-toggle\"><i class=\"far fa-times close\"></i></div>\r\n");
      out.write("                <div class=\"header__logo\"><a class=\"header-brand\" href=\"./home\"><img src=\"./img/core-img/logo.png\" alt=\"\"></a></div>\r\n");
      out.write("                <div class=\"menu-nav\"> \r\n");
      out.write("                    <ul class=\"header__nav-moblie\">\r\n");
      out.write("                        <li><a class=\"current\" href=\"home\">Home </a></li>\r\n");
      out.write("                        <li><a href=\"./vpop\">Nhạc Việt Nam </a></li>\r\n");
      out.write("                        <li><a href=\"./us-uk\">Nhạc Quốc Tế </a></li>\r\n");
      out.write("                        <li><a href=\"./lofi\">Lofi </a></li>\r\n");
      out.write("                        <li>\r\n");
      out.write("                            ");

                                if (session.getAttribute("user") == null) {
                            
      out.write(" \r\n");
      out.write("                            <br>\r\n");
      out.write("                            <a href=\"login.jsp\">Đăng nhập</a>\r\n");
      out.write("                            ");

                                }
                            
      out.write("\r\n");
      out.write("                        </li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("\r\n");
      out.write("                    ");
      out.write("\r\n");
      out.write("                    <div class=\"header__search\"> \r\n");
      out.write("                        <form value =\"txtSearch\" action=\"search\" method=\"post\">\r\n");
      out.write("                            <input type=\"text\" name=\"search\" placeholder=\"Search and hit enter...\">\r\n");
      out.write("                            <button class=\"btn\" type=\"submit\"> <i class=\"fa fa-search\"> </i></button>\r\n");
      out.write("                        </form>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("            ");
      out.write("                \r\n");
      out.write("            <div class=\"header__container container-fluid\">\r\n");
      out.write("                <div class=\"header__content\"> \r\n");
      out.write("                    <div class=\"header__logo\"><a class=\"header-brand\" href=\"./home\"><img src=\"./img/core-img/logo.png\" alt=\"\"></a></div>\r\n");
      out.write("                    <div class=\"header__menu\"> \r\n");
      out.write("                        <ul class=\"header__nav\">\r\n");
      out.write("                            ");
      if (_jspx_meth_c_if_0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("                                ");
      if (_jspx_meth_c_if_1(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("                            <li><a class=\"current\" href=\"./home\">Home </a></li>\r\n");
      out.write("                            <li class=\"nav-item dropdown\">\r\n");
      out.write("                                <a class=\"nav-link\" href=\"./vpop\">Nhạc Việt Nam </a>\r\n");
      out.write("                                <ul class=\"dropdown-menu\">\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${1}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc Trẻ</a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${2}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc Trữ Tình </a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${3}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc Cách Mạng </a></li>\r\n");
      out.write("                                </ul>\r\n");
      out.write("                            </li>\r\n");
      out.write("                            <li class=\"nav-item dropdown\">\r\n");
      out.write("                                <a class=\"nav-link\" href=\"./us-uk\">Nhạc Quốc Tế </a>\r\n");
      out.write("                                <ul class=\"dropdown-menu\">\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${4}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc US-UK</a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${5}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc KPOP </a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${6}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc Anime </a></li>\r\n");
      out.write("                                </ul>\r\n");
      out.write("                            </li>\r\n");
      out.write("                            <li class=\"nav-item dropdown\">\r\n");
      out.write("                                <a class=\"nav-link\" href=\"./lofi\">Lofi </a>\r\n");
      out.write("                                <ul class=\"dropdown-menu\">\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${8}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc Piano</a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${9}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc Guitar </a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item\" href=\"cate?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${7}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"> Nhạc Chill </a></li>\r\n");
      out.write("                                </ul>\r\n");
      out.write("                            </li>\r\n");
      out.write("                            <li><a class=\"option like\" href=\"./albums\">Albums </a></li>\r\n");
      out.write("                        </ul>\r\n");
      out.write("                        <div class=\"header__search\"> \r\n");
      out.write("                            <form value =\"txtSearch\" action=\"search\" method=\"post\">\r\n");
      out.write("                                <input type=\"text\" name=\"search\" placeholder=\"Search and hit enter...\">\r\n");
      out.write("                                <button class=\"btn\" type=\"submit\"> <i class=\"fa fa-search\"> </i></button>\r\n");
      out.write("                            </form>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <ul class=\"header__social\"> \r\n");
      out.write("                            <li><a class=\"fab fa-facebook\" href=\"#\"> </a></li>\r\n");
      out.write("                            <li><a class=\"fab fa-twitter\" href=\"#\"> </a></li>\r\n");
      out.write("                            <li><a class=\"fab fa-youtube\" href=\"#\"></a></li>\r\n");
      out.write("                            <li class=\"login\">\r\n");
      out.write("                                ");

                                    if (session.getAttribute("user") == null) {
                                
      out.write("                           \r\n");
      out.write("                                <a href=\"login.jsp\">Đăng nhập</a>\r\n");
      out.write("                                ");

                                    }
                                
      out.write("\r\n");
      out.write("                            </li>\r\n");
      out.write("\r\n");
      out.write("                            ");
      if (_jspx_meth_c_if_2(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\r\n");
      out.write("                        </ul>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"mobile-toggle\"><i class=\"far fa-bars open\"></i><i class=\"far fa-times close\">  </i></div>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"header__background\"></div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </section>\r\n");
      out.write("        <section id=\"slider\">\r\n");
      out.write("            <div class=\"slider__item bg-overlay bg-img slider__item-1 active\" style=\"background-image:  linear-gradient(315deg, #7f5a83 0%, #0d324d 74%);\">\r\n");
      out.write("                <div class=\"container\" style=\"margin-top: 150px;\">\r\n");
      out.write("                    <div class=\"slider__content\">\r\n");
      out.write("                        <div class=\"slider__wellcome\"> \r\n");
      out.write("                            <div class=\"slider__group--btn\">\r\n");
      out.write("                                <a href=\"home\"><div class=\"btn filter\">Back to Home</div></a>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <div class=\"slider__player player song-item\" data-path=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.slider.path}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" style=\"margin-top: -170px;\">\r\n");
      out.write("                            <!--                            max-width: 90%; max-height: 150px\"-->\r\n");
      out.write("                            <div class=\"player__img bg-img\" style=\"background-image: url('");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.slider.image}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("');\">\r\n");
      out.write("                                <div class=\"song-img--hover\"></div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"player__content\">\r\n");
      out.write("                                <div class=\"player__info\">\r\n");
      out.write("                                    <p class=\"player__date\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.slider.t_create}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</p>\r\n");
      out.write("                                    <h1 class=\"player__name song-name\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.slider.name}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</h1>\r\n");
      out.write("                                    <p class=\"player__text\"><span class=\"player__author\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.slider.author}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(" | </span><span class=\"player_duration\">00:");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.slider.duration}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</span></p>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"player__control\">\r\n");
      out.write("                                    <div class=\"play\"><i class=\"fas fa-play-circle\"></i></div>\r\n");
      out.write("                                    <div class=\"pause active\"><i class=\"fas fa-pause-circle\"></i></div><span class=\"currenTime\">00:00</span>\r\n");
      out.write("                                    <div class=\"sidebarTime--bg\">\r\n");
      out.write("                                        <div class=\"sidebarTime--current\"> </div>\r\n");
      out.write("                                    </div><span class=\"duration\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.slider.duration}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</span>\r\n");
      out.write("                                    <div class=\"volume\">\r\n");
      out.write("                                        <div class=\"volume__icon\"> <i class=\"mute fas fa-volume-slash\"></i><i class=\"volume--low fas fa-volume active\"></i><i class=\"volume--hight fas fa-volume-up\"></i></div>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                    <div class=\"volume__silebar--bg\">\r\n");
      out.write("                                        <div class=\"volume__silebar--current\"></div>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"like-share-download\">\r\n");
      out.write("                                    <div class=\"option like\"><i class=\"fas fa-heart\"> <span>Like</span></i></div>\r\n");
      out.write("                                    <div class=\"div\">\r\n");
      out.write("                                        <div class=\"option share\"> <i class=\"fas fa-share-alt\"> <span>Share</span></i></div>\r\n");
      out.write("                                        <div class=\"option download\">");
      if (_jspx_meth_c_if_3(_jspx_page_context))
        return;
      out.write("<i class=\"fas fa-download\"> <span>Download</span></i></a></div>\r\n");
      out.write("                                        </div>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div id=\"lyrics\">\r\n");
      out.write("                                <h2 class=\"song-name\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.slider.lyric}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</h2><h4 class=\"artist-name\"></h4>\r\n");
      out.write("                            <div id=\"lyrics-content\">\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                            \r\n");
      out.write("                    <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css\" rel=\"stylesheet\">\r\n");
      out.write("                    <section class=\"content-item\" id=\"comments\">\r\n");
      out.write("                        <div class=\"container\">   \r\n");
      out.write("                            <div class=\"row align-items-center\">\r\n");
      out.write("                                <div class=\"col-sm-8\">   \r\n");
      out.write("                                    <form action=\"addcomment\" method=\"get\">\r\n");
      out.write("                                        <h3 class=\"pull-left\">New Comment</h3>\r\n");
      out.write("                                        \r\n");
      out.write("                                        <fieldset>\r\n");
      out.write("                                            <input type=\"hidden\" name=\"id\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.slider.song_ID}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\">\r\n");
      out.write("                                            <div class=\"row\">\r\n");
      out.write("                                                <div class=\"col-sm-3 col-lg-2 hidden-xs\">\r\n");
      out.write("                                                    <img class=\"img-responsive\" src=\"https://bootdey.com/img/Content/avatar/avatar1.png\" alt=\"\">\r\n");
      out.write("                                                </div>\r\n");
      out.write("                                                <div class=\"form-group col-xs-12 col-sm-9 col-lg-10\">\r\n");
      out.write("                                                    <textarea class=\"form-control\" id=\"message\" name=\"comment\" placeholder=\"Your Comment\" required=\"\"></textarea>\r\n");
      out.write("                                                   \r\n");
      out.write("\r\n");
      out.write("                                                </div>\r\n");
      out.write("                                            </div>  \t\r\n");
      out.write("                                        </fieldset>\r\n");
      out.write("                                        <button type=\"submit\" class=\"btn btn-normal pull-right\">Submit</button>\r\n");
      out.write("                                    </form>\r\n");
      out.write("                                    \r\n");
      out.write("                            \r\n");
      out.write("\r\n");
      out.write("                                    ");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("                                    <!-- COMMENT 1 - END -->\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </section>\r\n");
      out.write("                    </div>\r\n");
      out.write("  \r\n");
      out.write("                    <style type=\"text/css\">\r\n");
      out.write("                        body{margin-top:20px;}\r\n");
      out.write("\r\n");
      out.write("                        .content-item {\r\n");
      out.write("                            padding:30px 0;\r\n");
      out.write("                            background-color:#FFFFFF;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        .content-item.grey {\r\n");
      out.write("                            background-color:#F0F0F0;\r\n");
      out.write("                            padding:50px 0;\r\n");
      out.write("                            height:100%;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        .content-item h2 {\r\n");
      out.write("                            font-weight:700;\r\n");
      out.write("                            font-size:35px;\r\n");
      out.write("                            line-height:45px;\r\n");
      out.write("                            text-transform:uppercase;\r\n");
      out.write("                            margin:20px 0;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        .content-item h3 {\r\n");
      out.write("                            font-weight:400;\r\n");
      out.write("                            font-size:20px;\r\n");
      out.write("                            color:#555555;\r\n");
      out.write("                            margin:10px 0 15px;\r\n");
      out.write("                            padding:0;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        .content-headline {\r\n");
      out.write("                            height:1px;\r\n");
      out.write("                            text-align:center;\r\n");
      out.write("                            margin:20px 0 70px;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        .content-headline h2 {\r\n");
      out.write("                            background-color:#FFFFFF;\r\n");
      out.write("                            display:inline-block;\r\n");
      out.write("                            margin:-20px auto 0;\r\n");
      out.write("                            padding:0 20px;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        .grey .content-headline h2 {\r\n");
      out.write("                            background-color:#F0F0F0;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        .content-headline h3 {\r\n");
      out.write("                            font-size:14px;\r\n");
      out.write("                            color:#AAAAAA;\r\n");
      out.write("                            display:block;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                        #comments {\r\n");
      out.write("                            box-shadow: 0 -1px 6px 1px rgba(0,0,0,0.1);\r\n");
      out.write("                            background-color:#FFFFFF;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments form {\r\n");
      out.write("                            margin-bottom:30px;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments .btn {\r\n");
      out.write("                            margin-top:7px;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments form fieldset {\r\n");
      out.write("                            clear:both;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments form textarea {\r\n");
      out.write("                            height:100px;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments .media {\r\n");
      out.write("                            border-top:1px dashed #DDDDDD;\r\n");
      out.write("                            padding:20px 0;\r\n");
      out.write("                            margin:0;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments .media > .pull-left {\r\n");
      out.write("                            margin-right:20px;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments .media img {\r\n");
      out.write("                            max-width:100px;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments .media h4 {\r\n");
      out.write("                            margin:0 0 10px;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments .media h4 span {\r\n");
      out.write("                            font-size:14px;\r\n");
      out.write("                            float:right;\r\n");
      out.write("                            color:#999999;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments .media p {\r\n");
      out.write("                            margin-bottom:15px;\r\n");
      out.write("                            text-align:justify;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments .media-detail {\r\n");
      out.write("                            margin:0;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments .media-detail li {\r\n");
      out.write("                            color:#AAAAAA;\r\n");
      out.write("                            font-size:12px;\r\n");
      out.write("                            padding-right: 10px;\r\n");
      out.write("                            font-weight:600;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments .media-detail a:hover {\r\n");
      out.write("                            text-decoration:underline;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments .media-detail li:last-child {\r\n");
      out.write("                            padding-right:0;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                        #comments .media-detail li i {\r\n");
      out.write("                            color:#666666;\r\n");
      out.write("                            font-size:15px;\r\n");
      out.write("                            margin-right:10px;\r\n");
      out.write("                        }\r\n");
      out.write("\r\n");
      out.write("                    </style>\r\n");
      out.write("\r\n");
      out.write("                 \r\n");
      out.write("\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </section>\r\n");
      out.write("\r\n");
      out.write("        <section id=\"newletter\">\r\n");
      out.write("            <div class=\"jarallax bg-overlay bg-img\" style=\"background-image: url('./img/core-img/jarallax.jpg');\">\r\n");
      out.write("                <div class=\"container newletter__container\">\r\n");
      out.write("                    <div class=\"newletter__content\">\r\n");
      out.write("                        <h1>Sign Up To Newsletter</h1>\r\n");
      out.write("                        <p>Subscribe to receive info on our latest news and episodes</p>\r\n");
      out.write("                    </div>\r\n");
      out.write("\r\n");
      out.write("                    <div class=\"newletter__subcribe\">\r\n");
      out.write("                        <form action=\"sendnew\" method=\"get\">\r\n");
      out.write("                            <input type=\"text\" placeholder=\"Your Email\" name=\"email\">\r\n");
      out.write("                            <input type=\"submit\" value=\"SUBCRIBE\">\r\n");
      out.write("                        </form>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </section>\r\n");
      out.write("        <section id=\"footer\">\r\n");
      out.write("            <div class=\"container footer__container\">\r\n");
      out.write("                <div class=\"footer__about\"> \r\n");
      out.write("                    <h2>About Us</h2>\r\n");
      out.write("                    <p>It is a long established fact that a reader will be distracted by the readable content.</p>\r\n");
      out.write("                    <p>&copy; Copyright &copy; 2022 <span>Group Four</span></p>\r\n");
      out.write("                </div>\r\n");
      out.write("                <ul class=\"footer__categories\"> \r\n");
      out.write("                    <h2>Categories</h2>\r\n");
      out.write("                    <li><a href=\"#\">Entrepreneurship </a></li>\r\n");
      out.write("                    <li><a href=\"#\">Media </a></li>\r\n");
      out.write("                    <li><a href=\"#\">Tech </a></li>\r\n");
      out.write("                    <li>   <a href=\"#\">Tutorials </a></li>\r\n");
      out.write("                </ul>\r\n");
      out.write("                <div class=\"footer_social\"> \r\n");
      out.write("                    <h2>Follow Us</h2>\r\n");
      out.write("                    <ul class=\"media\">\r\n");
      out.write("                        <li><a class=\"fab fa-facebook\" href=\"#\"> </a></li>\r\n");
      out.write("                        <li><a class=\"fab fa-twitter\" href=\"#\"> </a></li>\r\n");
      out.write("                        <li><a class=\"fab fa-pinterest\" href=\"#\"> </a></li>\r\n");
      out.write("                        <li><a class=\"fab fa-instagram\" href=\"#\"> </a></li>\r\n");
      out.write("                        <li><a class=\"fab fa-youtube\" href=\"#\"> </a></li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                    <ul class=\"store\"> \r\n");
      out.write("                        <li> <a href=\"\"><img src=\"./img/core-img/app-store.png\" alt=\"\"></a></li>\r\n");
      out.write("                        <li> <a href=\"\"><img src=\"./img/core-img/google-play.png\" alt=\"\"></a></li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </section>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <section id=\"toast\"></section>\r\n");
      out.write("        <audio id=\"audio\" src=\"\">    </audio>\r\n");
      out.write("        <script src=\"./node_modules/jarallax/dist/jarallax.min.js\"></script>\r\n");
      out.write("        <script src=\"./js/app.js\"></script>\r\n");
      out.write("        <script src=\"./js/jquery-3.2.1.min.js\"></script>\r\n");
      out.write("        <script src=\"./js/handle_ajax.js\"></script>\r\n");
      out.write("        <script src=\"./js/handle_toast.js\"></script>\r\n");
      out.write("        <script src=\"./js/lyric.js\"></script>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_set_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_scope_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_0.setPageContext(_jspx_page_context);
    _jspx_th_c_set_0.setParent(null);
    _jspx_th_c_set_0.setVar("user");
    _jspx_th_c_set_0.setScope("page");
    _jspx_th_c_set_0.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.user}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_set_0 = _jspx_th_c_set_0.doStartTag();
    if (_jspx_th_c_set_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_scope_nobody.reuse(_jspx_th_c_set_0);
      return true;
    }
    _jspx_tagPool_c_set_var_value_scope_nobody.reuse(_jspx_th_c_set_0);
    return false;
  }

  private boolean _jspx_meth_c_set_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_1 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_scope_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_1.setPageContext(_jspx_page_context);
    _jspx_th_c_set_1.setParent(null);
    _jspx_th_c_set_1.setVar("sliders");
    _jspx_th_c_set_1.setScope("page");
    _jspx_th_c_set_1.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.sliders}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_set_1 = _jspx_th_c_set_1.doStartTag();
    if (_jspx_th_c_set_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_scope_nobody.reuse(_jspx_th_c_set_1);
      return true;
    }
    _jspx_tagPool_c_set_var_value_scope_nobody.reuse(_jspx_th_c_set_1);
    return false;
  }

  private boolean _jspx_meth_c_set_2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_2 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_scope_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_2.setPageContext(_jspx_page_context);
    _jspx_th_c_set_2.setParent(null);
    _jspx_th_c_set_2.setVar("albums_Vpop");
    _jspx_th_c_set_2.setScope("page");
    _jspx_th_c_set_2.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.albums_Vpop}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_set_2 = _jspx_th_c_set_2.doStartTag();
    if (_jspx_th_c_set_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_scope_nobody.reuse(_jspx_th_c_set_2);
      return true;
    }
    _jspx_tagPool_c_set_var_value_scope_nobody.reuse(_jspx_th_c_set_2);
    return false;
  }

  private boolean _jspx_meth_c_set_3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_3 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_scope_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_3.setPageContext(_jspx_page_context);
    _jspx_th_c_set_3.setParent(null);
    _jspx_th_c_set_3.setVar("albums_USUK");
    _jspx_th_c_set_3.setScope("page");
    _jspx_th_c_set_3.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.albums_USUK}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_set_3 = _jspx_th_c_set_3.doStartTag();
    if (_jspx_th_c_set_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_scope_nobody.reuse(_jspx_th_c_set_3);
      return true;
    }
    _jspx_tagPool_c_set_var_value_scope_nobody.reuse(_jspx_th_c_set_3);
    return false;
  }

  private boolean _jspx_meth_c_set_4(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_4 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_scope_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_4.setPageContext(_jspx_page_context);
    _jspx_th_c_set_4.setParent(null);
    _jspx_th_c_set_4.setVar("albums_Lofi");
    _jspx_th_c_set_4.setScope("page");
    _jspx_th_c_set_4.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.albums_Lofi}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_set_4 = _jspx_th_c_set_4.doStartTag();
    if (_jspx_th_c_set_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_scope_nobody.reuse(_jspx_th_c_set_4);
      return true;
    }
    _jspx_tagPool_c_set_var_value_scope_nobody.reuse(_jspx_th_c_set_4);
    return false;
  }

  private boolean _jspx_meth_c_if_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_0.setPageContext(_jspx_page_context);
    _jspx_th_c_if_0.setParent(null);
    _jspx_th_c_if_0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.role_ID == 1}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_0 = _jspx_th_c_if_0.doStartTag();
    if (_jspx_eval_c_if_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("                                <li><a href=\"./listacc\">Manage Account</a></li>\r\n");
        out.write("                                ");
        int evalDoAfterBody = _jspx_th_c_if_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
    return false;
  }

  private boolean _jspx_meth_c_if_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_1 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_1.setPageContext(_jspx_page_context);
    _jspx_th_c_if_1.setParent(null);
    _jspx_th_c_if_1.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.role_ID == 3 || user.role_ID == 1}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_1 = _jspx_th_c_if_1.doStartTag();
    if (_jspx_eval_c_if_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("                                <li><a href=\"./manager\">Manage Songs</a></li>\r\n");
        out.write("                                ");
        int evalDoAfterBody = _jspx_th_c_if_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_1);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_1);
    return false;
  }

  private boolean _jspx_meth_c_if_2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_2 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_2.setPageContext(_jspx_page_context);
    _jspx_th_c_if_2.setParent(null);
    _jspx_th_c_if_2.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user != null}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_2 = _jspx_th_c_if_2.doStartTag();
    if (_jspx_eval_c_if_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("                                <li>\r\n");
        out.write("                                    <div class=\"user\"> \r\n");
        out.write("                                        <a href=\"UpdateProfile\"><div class=\"user__avatar bg-img\" style=\"background-image: url('./img/core-img/avatar-default-1.jpg');\"></div></a>\r\n");
        out.write("                                        <a class=\"fas fa-sign-out-alt icon\" href=\"signout\"></a>\r\n");
        out.write("                                        <div class=\"user__option\"> \r\n");
        out.write("                                            <div class=\"user__option-content\"> \r\n");
        out.write("                                                <div class=\"option-item view-info\">\r\n");
        out.write("                                                    <div class=\"user__avatar bg-img\" style=\"background-image: url('./img/core-img/avatar-default-1.jpg');\"></div>\r\n");
        out.write("                                                    <div class=\"user__info\"> \r\n");
        out.write("                                                        <div class=\"user__name heading\">");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.username}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write(" </div>\r\n");
        out.write("                                                        <div class=\"subtitle\">See your profile</div>\r\n");
        out.write("                                                    </div>\r\n");
        out.write("                                                </div>\r\n");
        out.write("                                                <div class=\"option-item setting\"><i class=\"fas fa-cog icon\"></i>\r\n");
        out.write("                                                    <div class=\"heading\">Settings </div>\r\n");
        out.write("                                                </div>\r\n");
        out.write("                                                <form action=\"sign\">\r\n");
        out.write("                                                    <input type=\"text\" name=\"url\" value=\"home\" hidden>\r\n");
        out.write("                                                    <label for=\"user__sign-out--pc\">\r\n");
        out.write("                                                        <div class=\"option-item logout\"><i class=\"fas fa-sign-out-alt icon\"></i>\r\n");
        out.write("                                                            <div class=\"heading\">Log Out</div>\r\n");
        out.write("                                                        </div>\r\n");
        out.write("                                                    </label>\r\n");
        out.write("                                                    <input type=\"submit\" hidden id=\"user__sign-out--pc\">\r\n");
        out.write("                                                </form>\r\n");
        out.write("                                            </div>\r\n");
        out.write("                                        </div>\r\n");
        out.write("                                    </div>\r\n");
        out.write("                                </li>\r\n");
        out.write("                            ");
        int evalDoAfterBody = _jspx_th_c_if_2.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_2);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_2);
    return false;
  }

  private boolean _jspx_meth_c_if_3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_3 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_3.setPageContext(_jspx_page_context);
    _jspx_th_c_if_3.setParent(null);
    _jspx_th_c_if_3.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user != null}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_3 = _jspx_th_c_if_3.doStartTag();
    if (_jspx_eval_c_if_3 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("<a href=\"");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.slider.path}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\" download>");
        int evalDoAfterBody = _jspx_th_c_if_3.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_3);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_3);
    return false;
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.cmm}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("cm");
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("                                        ");
          if (_jspx_meth_c_if_4((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
            return true;
          out.write("\r\n");
          out.write("                                    ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_if_4(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_4 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_4.setPageContext(_jspx_page_context);
    _jspx_th_c_if_4.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_0);
    _jspx_th_c_if_4.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${cm.status == 1}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_4 = _jspx_th_c_if_4.doStartTag();
    if (_jspx_eval_c_if_4 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("                                        ");
        if (_jspx_meth_c_forEach_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_4, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
          return true;
        out.write("\r\n");
        out.write("                                                    <p>");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${cm.comment}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("</p>\r\n");
        out.write("                                                    <ul class=\"list-unstyled list-inline media-detail pull-left\">\r\n");
        out.write("                                                        <li><i class=\"fa fa-calendar\"></i>");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${cm.tcreate}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("</li>\r\n");
        out.write("                                                        <li><i class=\"fa fa-thumbs-up\"></i>");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${cm.like}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("</li>\r\n");
        out.write("                                                    </ul>\r\n");
        out.write("                                                    <ul class=\"list-unstyled list-inline media-detail pull-right\">\r\n");
        out.write("                                                        <li class=\"\"><a href=\"\">LIKE</a></li>\r\n");
        out.write("                                                        <li class=\"\"><a href=\"\">Reply</a></li>\r\n");
        out.write("                                                    </ul>\r\n");
        out.write("                                                </div>\r\n");
        out.write("                                            </div>\r\n");
        out.write("                                        ");
        int evalDoAfterBody = _jspx_th_c_if_4.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_4);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_4);
    return false;
  }

  private boolean _jspx_meth_c_forEach_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_4, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_1 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_1.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_4);
    _jspx_th_c_forEach_1.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.user}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_1.setVar("a");
    int[] _jspx_push_body_count_c_forEach_1 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_1 = _jspx_th_c_forEach_1.doStartTag();
      if (_jspx_eval_c_forEach_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("                                            ");
          if (_jspx_meth_c_if_5((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_1, _jspx_page_context, _jspx_push_body_count_c_forEach_1))
            return true;
          out.write("\r\n");
          out.write("                                                    ");
          int evalDoAfterBody = _jspx_th_c_forEach_1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_1[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_1.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_1.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_1);
    }
    return false;
  }

  private boolean _jspx_meth_c_if_5(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_1, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_1)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_5 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_5.setPageContext(_jspx_page_context);
    _jspx_th_c_if_5.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_1);
    _jspx_th_c_if_5.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${a.user_ID == cm.userid}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_5 = _jspx_th_c_if_5.doStartTag();
    if (_jspx_eval_c_if_5 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("                                                <div class=\"media\">\r\n");
        out.write("                                                    <a class=\"pull-left\" href=\"#\"><img class=\"media-object\" src=\"https://bootdey.com/img/Content/avatar/avatar1.png\" alt=\"\"></a>\r\n");
        out.write("                                                    <div class=\"media-body\">\r\n");
        out.write("\r\n");
        out.write("                                                        <p class=\"media-heading\">");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${a.last_name}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("</p>\r\n");
        out.write("                                                    ");
        int evalDoAfterBody = _jspx_th_c_if_5.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_5);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_5);
    return false;
  }
}
