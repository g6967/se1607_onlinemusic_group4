<%-- 
    Document   : Header
    Created on : Jun 28, 2022, 10:26:11 PM
    Author     : Duy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="user" scope="page" value="${sessionScope.user}" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Loustic</title>
        <link rel="stylesheet" href="./css/app.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.1/css/all.css" type="text/css">
        
        <style>
             .slider__item .slider__content {
                margin-bottom: 500px;
            }
            .bg-overlay::after {
                position: absolute;
                content: "";
                height: 100%;
                width: 100%;
                top: 0;
                left: 0;
                z-index: -1;
            }
            @media all and (min-width: 992px) {
                .header__nav .nav-item .dropdown-menu{ display: none; }
                .header__nav .nav-item:hover .nav-link{   }
                .header__nav .nav-item:hover .dropdown-menu{ display: block; background-color: #e2264d }
                .header__nav .nav-item .dropdown-menu{ margin-top:0; }
            }
        </style>
    </head>
    <body>
       <%--Hearder--%>
        <section id="header" style="background: #000">
            <div class="header-mobile">
                <div class="mobile-toggle"><i class="far fa-times close"></i></div>
                <div class="header__logo"><a class="header-brand" href="./home"><img src="./img/core-img/logo.png" alt=""></a></div>
                <div class="menu-nav"> 
                    <ul class="header__nav-moblie">
                        <li>
                            <%
                                if (session.getAttribute("user") == null) {
                            %> 
                            <br>
                            <a href="login.jsp">Đăng nhập</a>
                            <%
                                }
                            %>
                        </li>
                    </ul>

                    <%--Search--%>
                    <div class="header__search"> 
                        <form value="txtSearch" action="search" method="post" style="display : flex">
                            <input type="text" name="search" placeholder="Search and hit enter..." style="margin-top: 10px">
                            <select name="searchType" style="height: 30px; margin-top: 12px">
                                <option value="1">Search By Name</option>
                                <option value="2">Search By Lyrics</option>
                                <option value="3">Search By Author</option>

                            </select>
                            <button class="btn" type="submit"> <i class="fa fa-search"> </i></button>
                        </form>
                    </div>
                </div>
            </div>
            <%--Hearder container--%>                
            <div class="header__container container-fluid">
                <div class="header__content" style="background-color: #000"> 
                    <div class="header__logo"><a class="header-brand" href="./home"><img src="./img/core-img/logo.png" alt=""></a></div>
                    <div class="header__menu"> 
                        <ul class="header__nav">
                            <c:if test="${user.role_ID == 1}">
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="./listacc">Manage Account </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="./manageAlbum">Manage Albums</a></li>
                                        <li><a class="dropdown-item" href="./manager">Manage Songs</a></li>
                                        <li><a class="dropdown-item" href="./managecomment">Manage Comment</a></li>
                                        <li><a class="dropdown-item" href="managehistory">Manage View History</a></li>
                                    </ul>
                                </li>
                            </c:if>
                            <c:if test="${user.role_ID == 3}">
                                <li><a href="./manager">Manage Songs</a></li>
                                </c:if>    
                            <li><a class="current" href="./home">Home </a></li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${1}">Nhạc Việt Nam </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${1}"> Nhạc Trẻ</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${2}"> Nhạc Trữ Tình </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${3}"> Nhạc Cách Mạng </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${2}">Nhạc Quốc Tế </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${4}"> Nhạc US-UK</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${5}"> Nhạc KPOP </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${6}"> Nhạc Anime </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${3}">Lofi </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${8}"> Nhạc Piano</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${9}"> Nhạc Guitar </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${7}"> Nhạc Chill </a></li>
                                </ul>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link" href="./albums">Albums </a>

                                <c:if test="${user != null}">
                                <li><a href="./allplaylist">View PlayList </a></li>                                
                                </c:if>
                            </li>
                            <c:if test="${user != null}">
                                <li><a href="history">Recently Visited</a></li>
                                </c:if>

                        </ul>
                        <div class="header__search"> 
                            <form value ="txtSearch" action="search" method="post" style="display: flex">
                                <input type="text" name="txtsearch" placeholder="Search and hit enter..." style="margin-top: 10px">
                                <select name="searchType" style="height: 30px; margin-top: 12px">
                                    <option value="1">Search By Name</option>
                                    <option value="2">Search By Lyrics</option>
                                    <option value="3">Search By Author</option>

                                </select>
                                <button class="btn" type="submit"> <i class="fa fa-search"> </i></button>
                            </form>
                        </div>
                        <ul class="header__social">      
                            <li class="login">
                                <%
                                    if (session.getAttribute("user") == null) {
                                %>                           
                                <a href="login.jsp">Đăng nhập</a>
                                <%
                                    }
                                %>
                            </li>

                            <c:if test="${user != null}">
                                <li>
                                    <div class="user"> 
                                        <a href="UpdateProfile"><div class="user__avatar bg-img" 
                                                                     <c:if test="${(user.avatar == null) or (user.avatar.isEmpty())}">
                                                                     style="background-image: url('./img/avatar.jpg');"
                                                                     </c:if>
                                                                     <c:if test="${user.avatar != null}">
                                                                     style="background-image: url('./img/uploads/${user.avatar}');"
                                                                     </c:if>
                                                                     ></div></a>
                                        <a class="fas fa-sign-out-alt icon" href="signout"></a>
                                        <div class="user__option"> 
                                            <div class="user__option-content"> 
                                                <div class="option-item view-info">
                                                    <div class="user__avatar bg-img" style="background-image: url('./img/core-img/avatar-default-1.jpg');"></div>
                                                    <div class="user__info"> 
                                                        <div class="user__name heading">${user.username} </div>
                                                        <div class="subtitle">See your profile</div>
                                                    </div>
                                                </div>
                                                <div class="option-item setting"><i class="fas fa-cog icon"></i>
                                                    <div class="heading">Settings </div>
                                                </div>
                                                <form action="sign">
                                                    <input type="text" name="url" value="home" hidden>
                                                    <label for="user__sign-out--pc">
                                                        <div class="option-item logout"><i class="fas fa-sign-out-alt icon"></i>
                                                            <div class="heading">Log Out</div>
                                                        </div>
                                                    </label>
                                                    <input type="submit" hidden id="user__sign-out--pc">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </c:if>

                        </ul>
                    </div>
                    <div class="mobile-toggle"><i class="far fa-bars open"></i><i class="far fa-times close">  </i></div>
                </div>
                <div class="header__background"></div>
            </div>
        </section>
        <c:if test="${user != null}">
            <section class="bg-overlay" id="user">
                <div class="user-container">
                    <div class="user-content">
                        <form action="./user/update?id=${user.user_ID}" method="POST">
                            <input type="text" name="url" value="/home" hidden>
                            <div class="user__base-info"> 
                                <div class="user_avatar bg-img" style="background-image: url('./img/core-img/avatar-default-1.jpg');"></div>
                                <div class="user__fullname"> 
                                    <div class="first_name"> 
                                        <div class="title">First name: </div>
                                        <div class="value"> <span class="active">${user.first_name}</span>
                                            <input class="edit_value" type="text" name="first-name" value="${user.first_name}" placeholder="First-name ...">
                                        </div><i class="fas fa-edit icon-setting active"> </i><i class="fal fa-window-close icon-setting-close"></i>
                                    </div>
                                    <div class="last_name"> 
                                        <div class="title">Last name: </div>
                                        <div class="value"> <span class="active">${user.last_name}</span>
                                            <input class="edit_value" type="text" name="last-name" value="${user.last_name}" placeholder="Last-name ...">
                                        </div><i class="fas fa-edit icon-setting active"> </i><i class="fal fa-window-close icon-setting-close"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="user__more-info">
                                <div class="user_username">
                                    <div class="title"> <i class="fas fa-user icon"></i><span>Username: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                    <div class="value"> 
                                        <div class="edit_value"></div><span class="active">${user.username}</span>
                                    </div>
                                </div>
                                <div class="user_password">
                                    <div class="title"> <i class="fas fa-key icon"></i><span>Password: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                    <div class="value"> <span class="active">********</span>
                                        <div class="edit_password edit_value">
                                            <input type="password" name="old-password" placeholder="Mật khẩu cũ">
                                            <input type="password" name="new-password" placeholder="Mật khẩu mới">
                                            <input type="password" name="verify-password" placeholder="Nhập lại mật khẩu mới">
                                        </div>
                                    </div>
                                </div>
                                <div class="user_email"> 
                                    <div class="title"> <i class="fas fa-envelope-open icon"></i><span>Email: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                    <div class="value"> <span class="active">${user.email}</span>
                                        <input class="edit_value" type="email" name="email" value="${user.email}" placeholder="Email ....">
                                    </div>
                                </div>
                                <div class="user_time-create">
                                    <div class="title"> <i class="fas fa-calendar-star icon"></i><span>Time create: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                    <div class="value"> 
                                        <div class="edit_value"></div><span class="active">${user.t_create}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="user__icon-summit"> 
                                <label for="user__edit_summit"><i class="fas fa-check-circle edit-summit"></i></label>
                                <input type="submit" hidden id="user__edit_summit">
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </c:if>
    </body>
</html>
