<%@page import="java.util.ArrayList"%>
<%@page import="model.Songs"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : home
    Created on : 27-06-2022
    Author     : duy
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:set var="user" scope="page" value="${sessionScope.user}" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Songs Page</title>
        <link rel="stylesheet" href="./css/app.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.1/css/all.css" type="text/css">
        <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <style>

        .slider__item .slider__content {
            margin-bottom: 500px;
        }
        @media all and (min-width: 992px) {
            .header__nav .nav-item .dropdown-menu{ display: none; }
            .header__nav .nav-item:hover .nav-link{   }
            .header__nav .nav-item:hover .dropdown-menu{ display: block; background-color: #e2264d }
            .header__nav .nav-item .dropdown-menu{ margin-top:0; }
        }
        select{
            background-color: #616161;
            color: white;
            border-radius: 10px;
        }
        .btn {
            position: relative;
            z-index: 1;
            height: 46px;
            line-height: 43px;
            font-size: 14px;
            font-weight: 600;
            display: inline-block;
            padding: 0 20px;
            text-align: center;
            text-transform: uppercase;
            border-radius: 30px;
            -webkit-transition: all 500ms;
            -o-transition: all 500ms;
            transition: all 500ms;
            border: 2px solid #f55656;
            letter-spacing: 1px;
            box-shadow: none;
            background-color: #f55656;
            color: #ffffff;
            cursor: pointer;
        }
        a, a:active, a:focus, a:hover {
            color: #232323;
            text-decoration: none;
            -webkit-transition-duration: 500ms;
            -o-transition-duration: 500ms;
            transition-duration: 500ms;
            outline: none;
            
         .btn-option.active {
            background-color: #ffffff;
            border: none;
            color: #000000;   
        }

    </style>
    <body>
        <jsp:include page="Header.jsp"/>      

        <%--post songs of album--%>
        <section id="recommend">
            <div class="container">
                <div class="group-tab__content">
                    <div class="tab__container">
                        <div class="tab__heading">
                            <h2>Bài Hát</h2>
<!--                            <div class="btn-group">
                                <div  class="btn btn-option btn-playAll"> <i  class="fas fa-play"> <span>Phát tất cả</span></i></div>
                                <div class="btn btn-option btn-playRandom"> <i class="fas fa-play"><span>Phát ngẫu nhiên</span></i></div>
                            </div>-->
                        </div>
                        <div class="tab__content vpop-tab active">
                            <div class="container__slide hide-on-mobile">
                                <div class="container__slide-show">
                                    <% int index = 0;%>
                                    <c:forEach var="album" items="${albums}">
                                        <% index++;%>
                                        <div class="container__slide-item <% switch (index) {
                                                case 1: %>first<% break;
                                                    case 2: %>second<% break;
                                                        case 3: %>third<% break;
                                                            default: %>fourth<% break;
                                                                }%>">
                                            <div class="container__slide-img" style="background: url('${album.image}') no-repeat center center / cover">
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>

                            <div class="tab__container-list">
                                <div class="play-list scroll-overflow">
                                    <c:forEach var="album" items="${albums}">
                                        <div class="song-item" data-path="${album.path}">
                                            <div class="song-content">
                                                <div class="song-img bg-img" style="background-image: url('${album.image}');">
                                                    <div class="song-img--hover"><i class="fas fa-play"></i></div>
                                                </div>
                                                <div class="song-info"> 
                                                    <h2 class="song-name"><a href="songDetail?id=${album.song_ID}">${album.name}</a></h2>
                                                    <div class="song-author">${album.author}</div>
                                                </div>
                                            </div>
                                            <div class="song-duration">${album.duration}</div>
                                            <div class="player__content">
                                                <div class="player__control"> 
                                                    <div class="play"><i class="fas fa-play-circle"></i></div>
                                                    <div class="pause active"><i class="fas fa-pause-circle"></i></div><span class="currenTime">00:00</span>
                                                    <div class="sidebarTime--bg">
                                                        <div class="sidebarTime--current"></div>
                                                    </div><span class="duration">${album.duration}</span>
                                                </div>
                                            </div>
                                            <div class="song-option"> 
                                                <div class="option download"><c:if test="${user != null}"><a href="${album.path}" download></c:if><i class="fal fa-arrow-alt-to-bottom"></i></a></div>
                                                <div <c:if test="${user != null}"> data-user_id="${user.user_ID}" data-song="${album.song_ID}"</c:if> class="option like active"> 
                                                    <a class="fas fa-heart" href="manageLike?songID=${album.song_ID}&operation=like&flag=0"  
                                                       <c:forEach items="${requestScope.checkLikeSong}" var="l">
                                                           <c:if test="${l.song_ID == album.song_ID}">style="color: red"</c:if>
                                                       </c:forEach>
                                                       > </a>
                                                    <a class="fas fa-heart-broken" href="manageLike?songID=${album.song_ID}&operation=like&flag=1"> </a>
                                                </div>
                                                <div>
                                                    <a href="addtoplaylists?songID=${album.song_ID}">Add playlist</a>
                                                </div>
                                                <div>
                                                    <a href="addonesongtoplaylist?son=${album.song_ID}"><i class="fas fa-solid fa-ellipsis-h"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>      
                        </div>


                    </div>
                </div>
            </div>
        </section>

        <%--newletter--%>
        <section id="newletter">
            <div class="jarallax bg-overlay bg-img" style="background-image: url('./img/core-img/jarallax.jpg');">
                <div class="container newletter__container">
                    <div class="newletter__content">
                        <h1>Sign Up To Newsletter</h1>
                        <p>Subscribe to receive info on our latest news and episodes</p>
                    </div>
                    <div class="newletter__subcribe">
                        <form action="" method="method">
                            <input type="text" placeholder="Your Email">
                            <input type="submit" value="SUBCRIBE">
                        </form>
                    </div>
                </div>
            </div>
        </div
    </section>
    <section id="footer">
        <div class="container footer__container" style="display: flex">
            <div class="footer__about"> 
                <h2>About Us</h2>
                <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                <p>&copy; Copyright &copy; 2022 <span>Group Four</span></p>
            </div>
            <ul class="footer__categories"> 
                <h2>Categories</h2>
                <li><a href="#" style="color: black">Entrepreneurship </a></li>
                <li><a href="#" style="color: black">Media </a></li>
                <li><a href="#" style="color: black">Tech </a></li>
                <li>   <a href="#" style="color: black">Tutorials </a></li>
            </ul>
            <div class="footer_social"> 
                <h2>Follow Us</h2>
                <ul class="media">
                    <li><a class="fab fa-facebook" href="#" style="color: black"> </a></li>
                    <li><a class="fab fa-twitter" href="#" style="color: black"> </a></li>
                    <li><a class="fab fa-pinterest" href="#" style="color: black"> </a></li>
                    <li><a class="fab fa-instagram" href="#" style="color: black"> </a></li>
                    <li><a class="fab fa-youtube" href="#" style="color: black"> </a></li>
                </ul>
                <ul class="store"> 
                    <li> <a href=""><img src="./img/core-img/app-store.png" alt=""></a></li>
                    <li> <a href=""><img src="./img/core-img/google-play.png" alt=""></a></li>
                </ul>
            </div>
        </div>
    </section>
    <c:if test="${user != null}">
        <section class="bg-overlay" id="user">
            <div class="user-container">
                <div class="user-content">
                    <form action="./user/update?id=${user.user_ID}" method="POST">
                        <input type="text" name="url" value="/home" hidden>
                        <div class="user__base-info"> 
                            <div class="user_avatar bg-img" style="background-image: url('./img/core-img/avatar-default-1.jpg');"></div>
                            <div class="user__fullname"> 
                                <div class="first_name"> 
                                    <div class="title">First name: </div>
                                    <div class="value"> <span class="active">${user.first_name}</span>
                                        <input class="edit_value" type="text" name="first-name" value="${user.first_name}" placeholder="First-name ...">
                                    </div><i class="fas fa-edit icon-setting active"> </i><i class="fal fa-window-close icon-setting-close"></i>
                                </div>
                                <div class="last_name"> 
                                    <div class="title">Last name: </div>
                                    <div class="value"> <span class="active">${user.last_name}</span>
                                        <input class="edit_value" type="text" name="last-name" value="${user.last_name}" placeholder="Last-name ...">
                                    </div><i class="fas fa-edit icon-setting active"> </i><i class="fal fa-window-close icon-setting-close"></i>
                                </div>
                            </div>
                        </div>
                        <div class="user__more-info">
                            <div class="user_username">
                                <div class="title"> <i class="fas fa-user icon"></i><span>Username: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                <div class="value"> 
                                    <div class="edit_value"></div><span class="active">${user.username}</span>
                                </div>
                            </div>
                            <div class="user_password">
                                <div class="title"> <i class="fas fa-key icon"></i><span>Password: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                <div class="value"> <span class="active">********</span>
                                    <div class="edit_password edit_value">
                                        <input type="password" name="old-password" placeholder="Mật khẩu cũ">
                                        <input type="password" name="new-password" placeholder="Mật khẩu mới">
                                        <input type="password" name="verify-password" placeholder="Nhập lại mật khẩu mới">
                                    </div>
                                </div>
                            </div>
                            <div class="user_email"> 
                                <div class="title"> <i class="fas fa-envelope-open icon"></i><span>Email: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                <div class="value"> <span class="active">${user.email}</span>
                                    <input class="edit_value" type="email" name="email" value="${user.email}" placeholder="Email ....">
                                </div>
                            </div>
                            <div class="user_time-create">
                                <div class="title"> <i class="fas fa-calendar-star icon"></i><span>Time create: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                <div class="value"> 
                                    <div class="edit_value"></div><span class="active">${user.t_create}</span>
                                </div>
                            </div>
                        </div>
                        <div class="user__icon-summit"> 
                            <label for="user__edit_summit"><i class="fas fa-check-circle edit-summit"></i></label>
                            <input type="submit" hidden id="user__edit_summit">
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </c:if>
    <section id="toast"></section>
    <audio id="audio" src="">    </audio>
    <script src="./node_modules/jarallax/dist/jarallax.min.js"></script>
    <script src="./js/app.js"></script>
    <script src="./js/jquery-3.2.1.min.js"></script>
    <script src="./js/handle_ajax.js"></script>
    <script src="./js/handle_toast.js"></script>

</body>
</html>

