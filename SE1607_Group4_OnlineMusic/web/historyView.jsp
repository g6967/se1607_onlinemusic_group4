<%-- 
    Document   : history
    Created on : Jun 14, 2022, 10:15:04 PM
    Author     : huanv
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>History View</title>

        <!--        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
                <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                <link href="assets/css/manager.css" rel="stylesheet" type="text/css"/>-->
        <!------------------------------------------------------------->
        <!--bs-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/manageSongs.css" rel="stylesheet" type="text/css"/>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#dtTableProduct').DataTable();
                $('.dataTables_length').addClass('bs-select');
            });
        </script>
    </head>
    <style>
        .table-title {
                background: gray;
            }
    </style>
    <body>


        <div class="container">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2><b>Recently Visited</b></h2>
                        </div>
                    </div>
                </div>
                <table id="dtTableProduct"  class="table table-striped table-hover ">
                    <thead>
                        <tr>
                            <th class="text-center">Image</th>
                            <th class="text-center">Song</th>
                            <th class="text-center">Author</th>
                            <th class="text-center">Duration</th>
                            <th class="text-center">Time Visited</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${requestScope.listHistory}" var="h">
                            <tr>
                                        <td class="text-center" style="vertical-align: middle"><img src="${h.image}" width="200" height="125"></td>
                                        <td class="text-center" style="vertical-align: middle"><b><a href="songDetail?id=${h.song_ID}">${h.name}</b></td>
                                        <td class="text-center" style="vertical-align: middle"><b>${h.author}</b></td>
                                        <td class="text-center" style="vertical-align: middle"><b>${h.duration}</b></td>
                                        <td class="text-center" style="vertical-align: middle"><b>${h.t_create}</b></td>
                            </tr>
                        </c:forEach>
                    </tbody>                  
                </table>
            </div>
            <a href="home"><button type="button" class="btn btn-danger">Back to home</button></a>
        </div>
        <script src="js/manager.js" type="text/javascript"></script>
    </body>
</html>

