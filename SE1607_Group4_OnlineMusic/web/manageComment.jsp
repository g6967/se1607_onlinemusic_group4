<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Manage Comment</title>
     
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/manageSongs.css" rel="stylesheet" type="text/css"/>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

        <style>
            table {
                width: 50%;
                counter-reset: 0;
            }
            table tr {
                counter-increment: row-num;
            }
            table tr td:first-child::before {
                content: counter(row-num) "";
            }
            .table-title {
                background: gray;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#dtTableProduct').DataTable();
                $('.dataTables_length').addClass('bs-select');
            });
        </script>
    </head>
    <body>


        <div class="container">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Manage <b>Comment</b></h2>
                        </div>
                        <div class="col-sm-6">
                            <a href="./blockAll"  class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Block All</span></a>
                        </div>
                    </div>
                </div>
                <h3 style="color: red">${mess}</h3>
                <table id="dtTableProduct"  class="table table-striped table-hover ">
                    <thead>
                        <tr>

                            <th class="text-center">No</th>
                            <th class="text-center">CommentID</th>
                            <th class="text-center">Comment</th>
                            <th class="text-center">SongName</th>
                            <th class="text-center">Customer</th>
                            <th class="text-center">Time create</th>
                            <th class="text-center">Time update</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${requestScope.list}" var="u">
                            <tr class="text-center">
                                <td style="vertical-align: middle"></td> 
                                <td style="vertical-align: middle">${u.commentid}</td>                                
                                <td style="vertical-align: middle">${u.comment}</td>
                                <c:forEach items="${requestScope.listS}" var="a">
                                    <c:if test="${a.song_ID == u.songid}">
                                        <td style="vertical-align: middle">${a.name}</td>
                                    </c:if>
                                </c:forEach>
                                
                                <c:forEach items="${requestScope.listU}" var="b">
                                    <c:if test="${b.user_ID == u.userid}">
                                        <td style="vertical-align: middle">${b.first_name} ${b.last_name}</td>
                                    </c:if>
                                </c:forEach>
                                
                                <td style="vertical-align: middle">${u.tcreate}</td>
                                <td style="vertical-align: middle">${u.tlastupdate}</td>                             
                                <td style="vertical-align: middle"><c:if test="${u.status == false}"><a>Block</a></c:if>
                                    <c:if test="${u.status == true}"><a>Unblock</a></c:if>
                                    </td>
                                    <td style="vertical-align: middle">
                                    <c:if test="${u.status == false}"><a href="unblockcomment?cid=${u.commentid}" class="btn btn-danger">UnBlock</a></c:if>
                                    <c:if test="${u.status == true}"><a href="blockcomment?cid=${u.commentid}" class="btn btn-success">Block</a></c:if>
                                    </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <a href="home"><button type="button" class="btn btn-danger">Back to home</button></a>
        </div>
        <script src="js/manager.js" type="text/javascript"></script>
    </body>
</html>
